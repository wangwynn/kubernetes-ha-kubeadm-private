# kubernetes-ha-kubeadm（已不可用）
> 可用版本请移步慕课网( **https://www.imooc.com** )的实战课：**《kubernetes(k8s)生产级实践指南 从部署到核心应用》**  
> 课程项目git地址： **https://git.imooc.com/coding-335/kubernetes-ha-kubeadm**

## 项目介绍
项目致力于让有意向使用原生kubernetes集群的企业或个人，可以方便的、系统的使用**kubeadm**的方式搭建kubernetes高可用集群。并且让相关的人员可
以更好的理解kubernetes集群的运作机制。
- **集群部署过程严格按照官方文档的流程。**
- **非科学上网的同学同样适用。**
- **持续跟进kubernetes最新版本**

